//
//  ViewController2.swift
//  Gestures
//
//  Created by David Davila on 26/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class ViewController2: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        UserDefaults.standard.set(true, forKey: "isLoggedIn");
        
    }
    
    @IBAction func LogOutButton(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: "isLoggedIn");
        self.dismiss(animated: true, completion: nil)
    }


}
