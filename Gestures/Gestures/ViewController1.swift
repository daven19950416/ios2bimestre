//
//  ViewController1.swift
//  Gestures
//
//  Created by David Davila on 26/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class ViewController1: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }

    override func viewDidAppear(_ animated: Bool) {
        guard let isLoggedIn = UserDefaults.standard.value(forKey: "isLoggedIn") as? Bool else {
            
            return
        }
        
        if isLoggedIn {
            performSegue(withIdentifier: "loginSegue", sender: self)
        }

    }
    
    @IBAction func salirAqui (segue:UIStoryboardSegue){
        UserDefaults.standard.set(false, forKey: "isLoggedIn");
        
    }

}
