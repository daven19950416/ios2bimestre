//
//  ViewController.swift
//  Gestures
//
//  Created by David Davila on 19/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var funcionLabel: UILabel!
    @IBOutlet weak var touchesLabel: UILabel!
    @IBOutlet weak var tapLabel: UILabel!
    
    @IBOutlet weak var postiionLabel: UILabel!
    @IBOutlet weak var vistaView: UIView!
    
    @IBOutlet var outletTapGesture: UITapGestureRecognizer!
    
    @IBOutlet weak var constrainAltura: NSLayoutConstraint!
    
    @IBOutlet weak var constraintAncho: NSLayoutConstraint!
    var orgAl, orgAn : CGFloat!;
    
    override func viewDidLoad() {
        
        
        super.viewDidLoad()
        
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func TapGesture(_ sender: UITapGestureRecognizer) {
        vistaView.backgroundColor = UIColor.blue
    }
    
    @IBAction func tapCoord(_ sender: UITapGestureRecognizer) {
        postiionLabel.text="asdadads"
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        funcionLabel.text="Touches Began"
        touchesLabel.text="\(touches.count)"
        let touch = touches.first
        tapLabel.text="\(touch!.tapCount)"
        
        
        
    }
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        funcionLabel.text="Touches Moved"
        let touch = touches.first
        let point = touch?.location(in: view)
        postiionLabel.text="\((point?.x)!),\((point?.y)!)"
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        funcionLabel.text="Touches Ended"
        touchesLabel.text = "0"
    }
    
    @IBAction func pinchOnView(_ sender: UIPinchGestureRecognizer) {
        print(sender.scale)
        
        
        
        constrainAltura.constant = orgAl * sender.scale
        constraintAncho.constant = orgAn * sender.scale
        
        if(sender.velocity > 8){
            constraintAncho.constant = view.frame.width
            constrainAltura.constant = view.frame.height
            sender.isEnabled = false
        }
        
        
        if(sender.state == .ended){
            orgAl = constrainAltura.constant
            orgAn = constraintAncho.constant        }
        
        if(sender.state == .cancelled){
            orgAl = constrainAltura.constant
            orgAn = constraintAncho.constant
            sender.isEnabled = true
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        orgAl = constrainAltura.constant
        orgAn = constraintAncho.constant
    }
}

