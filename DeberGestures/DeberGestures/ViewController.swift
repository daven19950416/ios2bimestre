//
//  ViewController.swift
//  DeberGestures
//
//  Created by David Davila on 6/7/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var funcionLabel: UILabel!
    @IBOutlet weak var touchesLabel: UILabel!
    @IBOutlet weak var tapLabel: UILabel!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var constraintAltura: NSLayoutConstraint!
    @IBOutlet weak var constraintAnchura: NSLayoutConstraint!
    
    @IBOutlet weak var vistaView: UIView!
    @IBOutlet var ouletTapGesture: UITapGestureRecognizer!
    var orgAl, orgAn : CGFloat!;
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        funcionLabel.text="Touches Began"
        touchesLabel.text="\(touches.count)"
        let touch = touches.first
        tapLabel.text="\(touch!.tapCount)"
        
        
        
    }
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        funcionLabel.text="Touches Moved"
        let touch = touches.first
        let point = touch?.location(in: view)
        positionLabel.text="\((point?.x)!),\((point?.y)!)"
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        funcionLabel.text="Touches Ended"
        touchesLabel.text = "0"
    }

    @IBAction func tapCoord(_ sender: UITapGestureRecognizer) {
    }
    
    override func viewWillAppear(_ animated: Bool) {
        orgAl = constraintAltura.constant
        orgAn = constraintAnchura.constant
    }

    @IBAction func pinchOnView(_ sender: UIPinchGestureRecognizer) {
        print(sender.scale)
        
        
        
        constraintAltura.constant = orgAl * sender.scale
        constraintAnchura.constant = orgAn * sender.scale
        
        if(sender.velocity > 8){
            constraintAltura.constant = view.frame.width
            constraintAltura.constant = view.frame.height
            
            sender.isEnabled = false
        }
        
        if(sender.velocity>5){
            vistaView.backgroundColor = UIColor.green
        }else{
            vistaView.backgroundColor = UIColor.cyan
        }
        
        
        if(sender.state == .ended){
            orgAl = constraintAltura.constant
            orgAn = constraintAnchura.constant        }
        
        if(sender.state == .cancelled){
            orgAl = constraintAltura.constant
            orgAn = constraintAnchura.constant
            sender.isEnabled = true
        }

    }
    @IBAction func TapGesture(_ sender: UITapGestureRecognizer) {
        vistaView.backgroundColor = UIColor.blue
    }
}

