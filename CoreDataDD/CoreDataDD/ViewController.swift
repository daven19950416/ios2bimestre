//
//  ViewController.swift
//  CoreDataDD
//
//  Created by David Davila on 10/7/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var nombreTextField: UITextField!
    
    @IBOutlet weak var direccionTextField: UITextField!
    
    @IBOutlet weak var telefonoTextField: UITextField!
    
    let objectManagedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    @IBAction func guardarBTN(_ sender: Any) {
        
        let entity = NSEntityDescription.entity(forEntityName: "Estudiante", in: objectManagedContext)
        
        let estudiante = Estudiante(entity: entity!, insertInto: objectManagedContext)
        
        estudiante.nombre = nombreTextField.text!
        estudiante.telefono = telefonoTextField.text!
        estudiante.direccion = direccionTextField.text!
        
        do{
            try objectManagedContext.save()
            
        }catch let error{
            
            print (error)
        }
   
    }
    
    @IBAction func consultarDatosBtn(_ sender: Any) {
        
        let request : NSFetchRequest<Estudiante> = Estudiante.fetchRequest()
        
        do{
            
            let results = try objectManagedContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>)
            
            for object in results {
                let est = object as! Estudiante
                print ("\(est.nombre!), \(est.telefono!), \(est.direccion!)")
            }
            
        }catch let error{
            
            print (error)
        }
        
        
    }
    
    @IBAction func buscarBtn(_ sender: Any) {
        let request : NSFetchRequest<Estudiante> = Estudiante.fetchRequest()
        let entity = NSEntityDescription.entity(forEntityName: "Estudiante", in: objectManagedContext)
        request.entity = entity
        
        let predicate = NSPredicate(format: "(nombre = %@)", nombreTextField.text!)
        
        request.predicate = predicate
        
        do{
            
            let results = try objectManagedContext.fetch(request as! NSFetchRequest<NSFetchRequestResult>)
            
            guard let est = results.first as? Estudiante else {
                
                direccionTextField.text = "n/a"
                telefonoTextField.text = "n/a"
                
                return
            }
            
            direccionTextField.text = est.direccion
            telefonoTextField.text = est.telefono
            
        } catch let error{
            print (error)
        }
        
        
        
    }
    
}
