//
//  BackendManager.swift
//  
//
//  Created by David Davila on 7/8/17.
//
//

import Foundation
import Alamofire
import AlamofireImage
import AlamofireObjectMapper

//Clase que permite consumir datos de API y BackEnd

class BackendManager {
    
    // GET: - Moteles de la API
    func getAllMotel () {
        
        let url = ""
        
        Alamofire.request(url).responseArray { (response: DataResponse<[Motel]>) in
            
            let motelResponse = response.result.value
            
            if let motelArray = motelResponse{
                NotificationCenter.default.post(name: NSNotification.Name("getMotel"), object: nil, userInfo: [ "motelArray" : motelArray ])
            }
            
        }
        
    }
    
    func getAllHab(){
        //FUNCION PARA LLENAR HABITACIONS
    }
    
    //GET: - Imagen del objeto Json de API
    func getImage (_ url : String, completionHandler: @escaping(UIImage)->()){
        Alamofire.request(url).responseImage { response in
            if let image = response.result.value {
                completionHandler(image)
            }
        }
}
}
