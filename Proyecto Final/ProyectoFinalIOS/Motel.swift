//
//  Motel.swift
//  
//
//  Created by David Davila on 7/8/17.
//
//

import Foundation
import ObjectMapper

// Clase que permite mapear de un objeto JSON a un objeto Swift

class Motel : Mappable {
    
    var motelNombre: String?
    var motelDescripcion: String?
    var motelDireccion: String?
    var motelLinkImagenPortada: String?
    var motelImagenPortada: UIImage?
    var motelLinkImagenLogo: String?
    var motelImagenLogo: UIImage?
    
    
    required init?(map: Map) {
        
    }
    
    func mapping (map: Map) {
        motelNombre <- map ["nombre"]
        motelDescripcion <- map ["descripcion"]
        motelDireccion <- map ["direccion"]
        motelLinkImagenPortada <- map ["linkPortada"]
        motelLinkImagenLogo <- map ["linkLogo"]
    }
}
