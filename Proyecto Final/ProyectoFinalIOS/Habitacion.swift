//
//  Habitacion.swift
//  ProyectoFinalIOS
//
//  Created by David Davila on 7/8/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import Foundation

import ObjectMapper

class Habitacion : Mappable {
    
    var habNombre: String?
    var habDescripcion: String?
    var habPrecio: String?
    var habDisponibles: String?
    var habImagenPortada: UIImage?
    var habLinkImagenPortada: String?
    var habImagenCarrusel1: UIImage?
    var habLinkImagenCarrusel1: String?
    var habImagenCarrusel2: UIImage?
    var habLinkImagenCarrusel2: String?
    var habImagenCarrusel3: UIImage?
    var habLinkImagenCarrusel3: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping (map: Map) {
        habNombre <- map ["hnombre"]
        habDescripcion <- map ["hdescripcion"]
        habPrecio <- map ["hPrecio"]
        habDisponibles <- map ["hDisponibles"]
        habLinkImagenPortada <- map ["hportada"]
        habLinkImagenCarrusel1 <- map ["hcarrusel1"]
        habLinkImagenCarrusel2 <- map ["hcarrusel2"]
        habLinkImagenCarrusel3 <- map ["hcarrusel3"]
        
}
}
