//
//  ListaMotelesViewController.swift
//  ProyectoFinalIOS
//
//  Created by David Davila on 31/7/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class ListaMotelesViewController:UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate,UISearchResultsUpdating {
    var resultSearchController:UISearchController!
    
    var arrayMotel:[Motel] = []
    @IBOutlet weak var motelTableView: UITableView!
    var motelFiltrado:[Motel]=[]
    var isSearchActive = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let bm = BackendManager()
        bm.getAllMotel()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: NSNotification.Name("getMotel"), object: nil)
        confSearchBar()
        // Do any additional setup after loading the view.
    }
    func reloadData(_ notification:Notification) {
        guard let arrayMotel = notification.userInfo?["motelArray"] as? [Motel] else {
            return
        }
        self.arrayMotel = arrayMotel
        motelTableView.reloadData()
    }
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearchActive{
            return motelFiltrado.count
        } else {
            return arrayMotel.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.motelTableView.dequeueReusableCell(withIdentifier: "motelCell") as? MotelTableViewCell
        
        if isSearchActive{
            cell?.motelObj = motelFiltrado[indexPath.row]
        }else{
            cell?.motelObj = self.arrayMotel[indexPath.row]
        }
        
        
        cell?.fillData()
        return cell!
    }
    
    func confSearchBar(){
        resultSearchController = UISearchController(searchResultsController: nil)
        resultSearchController.searchResultsUpdater = self
        resultSearchController.hidesNavigationBarDuringPresentation = false
        resultSearchController.dimsBackgroundDuringPresentation = false
        resultSearchController.searchBar.searchBarStyle = .prominent
        resultSearchController.searchBar.sizeToFit()
        motelTableView.tableHeaderView = resultSearchController.searchBar
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        var searchText = searchController.searchBar.text?.lowercased()
        
        if searchController.searchBar.text !=  ""{
            
            
            isSearchActive = true
            motelFiltrado = arrayMotel.filter{
                ($0.motelNombre?.lowercased().contains(searchText!))!
            }
            
        } else {
            isSearchActive = false
        }
        motelTableView.reloadData()
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        isSearchActive = true
        
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearchActive = false
        motelTableView.reloadData()
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "mostrarDetallesMotel"{
            let destinationController = segue.destination as! ListaHabitacionesViewController
            
            
            if let indexPath = self.motelTableView.indexPathForSelectedRow {
                let selectedMotel = arrayMotel[indexPath.row]
                destinationController.currentMotel = selectedMotel
                
            }
        }
}
}
