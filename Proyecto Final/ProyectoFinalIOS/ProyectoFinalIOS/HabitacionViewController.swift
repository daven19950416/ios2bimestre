//
//  HabitacionViewController.swift
//  ProyectoFinalIOS
//
//  Created by David Davila on 7/8/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class HabitacionViewController: UIViewController {
 
    var currentHab : Habitacion!
    
    //Informacion del Carrusel 
    //https://github.com/Alan881/AACarousel
    
    
    @IBOutlet weak var nombreHabLabel: UILabel!
    @IBOutlet weak var precioHabLabel: UILabel!
    
    @IBOutlet weak var habDisponiblesLabel: UILabel!
    
    @IBOutlet weak var descripcionLabel: UILabel!
    
    @IBAction func reservarBoton(_ sender: UIButton) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Llenar Inofrmacion de la Habitacion
        nombreHabLabel.text = currentHab.habNombre
        precioHabLabel.text = currentHab.habPrecio
        habDisponiblesLabel.text = currentHab.habDisponibles
        descripcionLabel.text = currentHab.habDescripcion
    
        //LLENAR LA INFO DE LAS IMAGENES
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
}
