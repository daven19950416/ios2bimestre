//
//  ListaHabitacionesViewController.swift
//  ProyectoFinalIOS
//
//  Created by David Davila on 7/8/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class ListaHabitacionesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{

    @IBOutlet weak var habTableView: UITableView!
    @IBOutlet weak var portadaMotel: UIImageView!
    
    @IBOutlet weak var LogoMotel: UIImageView!
    
    @IBOutlet weak var NombreMotelLabel: UILabel!
    
    @IBOutlet weak var DireccionMotelLabel: UILabel!
    
    
    var currentMotel : Motel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Llenando informacion Motel - Estatico
        NombreMotelLabel.text = currentMotel.motelNombre!
        DireccionMotelLabel.text = currentMotel.motelDireccion
        if currentMotel.motelImagenPortada == nil {
            
            let bm = BackendManager()
            
            bm.getImage((currentMotel?.motelLinkImagenPortada)!, completionHandler: { (imageR) in
                
                DispatchQueue.main.async {
                    self.portadaMotel.image = imageR
                }
                
            })
            
        } else {
            
            portadaMotel.image = currentMotel.motelImagenPortada
            
        }
        if currentMotel.motelImagenLogo == nil {
            
            let bm = BackendManager()
            
            bm.getImage((currentMotel?.motelLinkImagenLogo)!, completionHandler: { (imageR) in
                
                DispatchQueue.main.async {
                    self.LogoMotel.image = imageR
                }
                
            })
            
        } else {
            
            LogoMotel.image = currentMotel.motelImagenLogo
            
        }
        
        //Para llenar las celdas
        let bm = BackendManager()
        bm.getAllHab()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: NSNotification.Name("getHab"), object: nil)

        
        
    }

    var arrayHab:[Habitacion] = []
    
    func reloadData(_ notification:Notification) {
        guard let arrayHab = notification.userInfo?["habArray"] as? [Habitacion] else {
            return
        }
        self.arrayHab = arrayHab
        habTableView.reloadData()
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
            return arrayHab.count
            }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.habTableView.dequeueReusableCell(withIdentifier: "habCell") as? HabitacionTableViewCell
        
                    cell?.habObj = self.arrayHab[indexPath.row]
        
        
        
        cell?.fillData()
        
        return cell!
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "mostrarDetallesHabitacion"{
            let destinationController = segue.destination as! HabitacionViewController
            
            
            if let indexPath = self.habTableView.indexPathForSelectedRow {
                let selectedHab = arrayHab[indexPath.row]
                destinationController.currentHab = selectedHab
                
            }
        }
    }

}
