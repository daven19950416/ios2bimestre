//
//  HabitacionTableViewCell.swift
//  ProyectoFinalIOS
//
//  Created by David Davila on 7/8/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class HabitacionTableViewCell: UITableViewCell {

    @IBOutlet weak var habitacionPortada: UIImageView!
    
    @IBOutlet weak var nombreHabitacionLabel: UILabel!
    
    @IBOutlet weak var descripcionHabitacionLabel: UILabel!
    
    @IBOutlet weak var precioHabitacionLabel: UILabel!
    
    var habObj : Habitacion!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func fillData() {
        //Llenar datos de los labels y de imagenes
        self.nombreHabitacionLabel.text = self.habObj.habNombre
        self.descripcionHabitacionLabel.text = self.habObj.habDescripcion
        self.precioHabitacionLabel.text = self.habObj.habPrecio
        //Llenar la portada con el link de la img
        if habObj.habImagenPortada == nil {
            
            let bm = BackendManager()
            
            bm.getImage((habObj?.habLinkImagenPortada)!, completionHandler: { (imageR) in
                
                DispatchQueue.main.async {
                    self.habitacionPortada.image = imageR
                }
                
            })
            
        } else {
            
            habitacionPortada.image = habObj.habImagenPortada
            
        }
    }

    
}
