//
//  MotelTableViewCell.swift
//  ProyectoFinalIOS
//
//  Created by David Davila on 7/8/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class MotelTableViewCell: UITableViewCell  {
    
    
    @IBOutlet weak var NombreMotelLabel: UILabel!
    
    @IBOutlet weak var LogoMotel: UIImageView!
    
    @IBOutlet weak var portadaMotel: UIImageView!

    
   // @IBAction func RankingMotel(_ sender: HCSStarRatingView) {
    
        //Llenar conforme al Ranking
    //https://github.com/hsousa/HCSStarRatingView
    //}
    
    
    var motelObj:Motel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
  

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func fillData() {
    //Llenar datos de los labels y de imagenes
            self.NombreMotelLabel.text = self.motelObj.motelNombre
            //Llenar la portada con el link de la img
            if motelObj.motelImagenPortada == nil {
                
                let bm = BackendManager()
                
                bm.getImage((motelObj?.motelLinkImagenPortada)!, completionHandler: { (imageR) in
                    
                    DispatchQueue.main.async {
                        self.portadaMotel.image = imageR
                    }
                    
                })
                
            } else {
                
                portadaMotel.image = motelObj.motelImagenPortada
                
            }
        //Llenar el logo con el link de la img
        if motelObj.motelImagenLogo == nil {
            
            let bm = BackendManager()
            
            bm.getImage((motelObj?.motelLinkImagenLogo)!, completionHandler: { (imageR) in
                
                DispatchQueue.main.async {
                    self.LogoMotel.image = imageR
                }
                
            })
            
        } else {
            
            LogoMotel.image = motelObj.motelImagenLogo
            
        }
        }

    
}
