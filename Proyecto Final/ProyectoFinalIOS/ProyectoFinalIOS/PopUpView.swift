//
//  PopUpView.swift
//  ProyectoFinalIOS
//
//  Created by David Davila on 7/8/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

@IBDesignable class PopUpView: UIView {

    @IBInspectable var cornerRadius: CGFloat = 0{
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
 
}
